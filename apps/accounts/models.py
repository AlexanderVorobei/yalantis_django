from django.db import models
from django.contrib.auth.models import AbstractUser
from .managers import UserManager


class User(AbstractUser):
    email = models.EmailField(verbose_name="email", max_length=254, unique=True)
    phone = models.CharField(
        verbose_name="mobile phone", max_length=17, blank=True, null=True
    )
    password_expire_at = models.DateTimeField(
        verbose_name="expired at", null=True, blank=True
    )
    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email"]
    objects = UserManager()

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = "user"
        verbose_name_plural = "users"
