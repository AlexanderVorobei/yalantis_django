from rest_framework.reverse import reverse, reverse_lazy
from rest_framework.test import APITestCase
from apps.accounts.models import User
from apps.dialogs.models import Thread, Message


class UserMixin(object):
    @staticmethod
    def _create_user(user_number: int):
        user = User.objects.create(
            username=f"test{user_number}",
            first_name=f"Name{user_number}",
            last_name=f"Lname{user_number}",
            email=f"test{user_number}@example.com",
        )
        user.set_password(f"testPassword")
        user.save()
        return user


class ThreadTestCase(APITestCase, UserMixin):
    def setUp(self) -> None:
        self.user_1 = self._create_user(user_number=1)
        self.user_2 = self._create_user(user_number=2)
        self.user_3 = self._create_user(user_number=3)

        self.login_url = reverse("super_login")
        self.thread_list_url = reverse("dialogs:thread-list")
        self.thread_detail_url = reverse("dialogs:thread-detail", kwargs={"pk": 1})

        self.thread = Thread.objects.create()
        self.thread.participants.set([self.user_1.id, self.user_2.id])

        self.message_1 = Message.objects.create(
            text="hello", thread=self.thread, sender=self.thread.participants.first()
        )

    def _get_user_token(self, username="test", password="testPassword"):
        response = self.client.post(
            self.login_url,
            {"username": username, "password": password},
            format="json",
        )
        response_data = response.json()
        return "JWT {}".format(response_data.get("token", ""))

    def test_create_thread(self):
        # Thread creation test
        result = self.client.post(
            self.thread_list_url,
            {"participants": [self.user_1.id, self.user_2.id]},
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 201)
        # Thread create by an unauthorized user
        result = self.client.post(
            self.thread_list_url, {"participants": [self.user_1.id, self.user_2.id]}
        )
        self.assertEqual(result.status_code, 401)
        # Thread create with a three participants
        result = self.client.post(
            self.thread_list_url,
            {"participants": [self.user_1.id, self.user_2.id, self.user_3.id]},
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 400)
        # Thread create without an authorized user among the participants
        result = self.client.post(
            self.thread_list_url,
            {"participants": [self.user_2.id, self.user_3.id]},
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 400)
        # Thread create with a one participants
        result = self.client.post(
            self.thread_list_url,
            {"participants": [self.user_1.id]},
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 400)

    def test_list_threads(self):
        # Get lists of threads by an unauthorized user
        result = self.client.get(self.thread_list_url)
        self.assertEqual(result.status_code, 401)
        # Get lists of threads
        result = self.client.get(
            self.thread_list_url,
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 200)

        data = result.json()
        # Checking the number of issued threads
        self.assertEqual(len(data), 1)
        # Checking the id of the created thread
        self.assertEqual(data[0]["id"], 1)
        # Checking the last message
        self.assertTrue(data[0]["last_message"])
        self.assertEqual(data[0]["last_message"].get("text"), "hello")
        # Checking the receipt of the number of unread messages
        self.assertEqual(data[0]["unreaded_messages"], 1)
        message = Message.objects.filter(id=1).first()
        message.is_read = True
        message.save()
        result = self.client.get(
            self.thread_list_url,
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        data = result.json()
        self.assertEqual(data[0]["unreaded_messages"], 0)

    def test_update_thread(self):
        # Thread update test (participants deletion)
        # Delete participant by an unauthorized user
        result = self.client.patch(
            self.thread_detail_url, HTTP_AUTHORIZATION=self._get_user_token()
        )
        self.assertEqual(result.status_code, 401)
        # Delete participant by an authorized user
        result = self.client.patch(
            self.thread_detail_url,
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 200)
        # Delete participant and check if the thread is delete
        result = self.client.patch(
            self.thread_detail_url,
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_2.username),
        )
        self.assertEqual(result.status_code, 200)
        result = self.client.get(
            self.thread_list_url,
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        data = result.json()
        # Checking the number of issued threads
        self.assertEqual(len(data), 0)

    def test_detail_thread(self):
        # Test getting detailed information about a thread by id
        result = self.client.get(
            self.thread_detail_url,
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 200)
        # Testing of obtaining detailed information about a thread
        # by an authorized user who is not among the participants
        result = self.client.get(
            self.thread_detail_url,
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_3.username),
        )
        self.assertEqual(result.status_code, 404)
        # Getting detailed information about thread by an unauthorized user
        result = self.client.get(
            self.thread_detail_url, HTTP_AUTHORIZATION=self._get_user_token()
        )
        self.assertEqual(result.status_code, 401)


class MessageTestCase(APITestCase, UserMixin):
    def setUp(self) -> None:
        self.user_1 = self._create_user(user_number=1)
        self.user_2 = self._create_user(user_number=2)
        self.user_3 = self._create_user(user_number=3)

        self.login_url = reverse("super_login")
        self.message_list_url = reverse_lazy("dialogs:message-list")
        self.message_detail_url = reverse("dialogs:message-detail", kwargs={"pk": 1})

        self.thread = Thread.objects.create()
        self.thread.participants.set([self.user_1.id, self.user_2.id])

        self.message = Message.objects.create(
            text="hello", thread=self.thread, sender=self.thread.participants.first()
        )

    def _get_user_token(self, username="test", password="testPassword"):
        response = self.client.post(
            self.login_url,
            {"username": username, "password": password},
            format="json",
        )
        response_data = response.json()
        return "JWT {}".format(response_data.get("token", ""))

    def test_create_message(self):
        # Message creation test
        result = self.client.post(
            self.message_list_url,
            {"text": "hello", "thread": 1},
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 201)
        # Message create by an unauthorized user
        result = self.client.post(self.message_list_url, {"text": "hello", "thread": 1})
        self.assertEqual(result.status_code, 401)
        # Message create without an authorized user among the participants
        result = self.client.post(
            self.message_list_url,
            {"text": "hello", "thread": 1},
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_3.username),
        )
        self.assertEqual(result.status_code, 400)

    def test_list_message(self):
        # Get lists of messages by an unauthorized user
        result = self.client.get("%s?thread=%s" % (self.message_list_url, "1"))
        self.assertEqual(result.status_code, 401)
        # Get lists of messages without an authorized user among the participants
        result = self.client.get(
            "%s?thread=%s" % (self.message_list_url, "1"),
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_3.username),
        )
        self.assertEqual(result.status_code, 404)
        # Checking message status before receiving
        self.assertFalse(self.message.is_read)
        # Test for getting a list of messages
        result = self.client.get(
            "%s?thread=%s" % (self.message_list_url, "1"),
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 200)

        data = result.json()
        # Checking the number of messages issued
        self.assertEqual(len(data), 1)
        # Checking the id of the created message
        self.assertEqual(data[0]["id"], 1)
        # Checking message status after receiving
        self.assertTrue(data[0]["is_read"])

    def test_update_message(self):
        # Message edit by an unauthorized user
        result = self.client.patch(
            "%s?thread=%s" % (self.message_detail_url, "1"), {"text": "Django"}
        )
        self.assertEqual(result.status_code, 401)
        # Message edit by a user other than the sender
        result = self.client.patch(
            "%s?thread=%s" % (self.message_detail_url, "1"),
            {"text": "Django"},
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_2.username),
        )
        self.assertEqual(result.status_code, 403)
        # Message edit
        result = self.client.patch(
            "%s?thread=%s" % (self.message_detail_url, "1"),
            {"text": "Django"},
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 200)

        data = result.json()
        # Checking the modified message
        self.assertEqual(data["text"], "Django")

    def test_delete_message(self):
        # Message delete by an unauthorized user
        result = self.client.delete("%s?thread=%s" % (self.message_detail_url, "1"))
        self.assertEqual(result.status_code, 401)
        # Message delete by a user other than the sender
        result = self.client.delete(
            "%s?thread=%s" % (self.message_detail_url, "1"),
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_2.username),
        )
        self.assertEqual(result.status_code, 403)
        # Message delete
        result = self.client.delete(
            "%s?thread=%s" % (self.message_detail_url, "1"),
            HTTP_AUTHORIZATION=self._get_user_token(username=self.user_1.username),
        )
        self.assertEqual(result.status_code, 204)
