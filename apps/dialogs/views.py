from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.viewsets import ModelViewSet
from .models import Thread, Message
from .permissions import IsParticipants, IsSenderOrReadOnly
from .serializers import (
    ThreadSerializer,
    ThreadListSerializer,
    MessageSerializer,
    MessageListSerializer,
)


class ThreadViewSet(ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsParticipants,
    ]

    def get_queryset(self):
        return Thread.objects.filter(participants=self.request.user)

    def get_serializer_class(self):
        return ThreadListSerializer if self.action == "list" else ThreadSerializer


class MessageViewSet(ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsSenderOrReadOnly,
    ]

    def get_queryset(self):
        thread_pk = self.request.GET.get("thread")
        thread = Thread.objects.filter(id=thread_pk).first()
        queryset = Message.objects.filter(thread=thread)
        if not thread_pk:
            raise NotFound(detail="No thread selected.", code=HTTP_204_NO_CONTENT)
        if not thread:
            raise NotFound(detail="Thread not found.", code=HTTP_204_NO_CONTENT)
        if self.request.user.email not in thread.get_participants():
            raise NotFound(detail="Thread not found.", code=HTTP_204_NO_CONTENT)
        if not queryset:
            raise NotFound(
                detail="There are not message yet.", code=HTTP_204_NO_CONTENT
            )
        return queryset

    def get_serializer_class(self):
        return MessageListSerializer if self.action == "list" else MessageSerializer
