from django.db import models
from apps.accounts.models import User
from .managers import MessageManager


class Thread(models.Model):
    participants = models.ManyToManyField(User, related_name="participants")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "thread"
        verbose_name_plural = "threads"
        ordering = ("pk",)

    def __str__(self):
        return f"Thread: {self.pk}"

    def get_participants(self):
        return "; ".join([str(p) for p in self.participants.all()])


class Message(models.Model):
    text = models.TextField(max_length=4096)
    sender = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE, related_name="thread")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_read = models.BooleanField(default=False)
    objects = MessageManager()

    class Meta:
        verbose_name = "message"
        verbose_name_plural = "messages"
        ordering = ("id",)

    def __str__(self):
        return f"{self.sender}: {self.text}"
